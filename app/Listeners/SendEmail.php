<?php

namespace App\Listeners;

use App\Events\VideoCreaed;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\VideoCreaed  $event
     * @return void
     */
    public function handle(VideoCreaed $event)
    {
        dump('This is send email listener');
    }
}
