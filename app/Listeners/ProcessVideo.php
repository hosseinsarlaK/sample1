<?php

namespace App\Listeners;

use App\Events\VideoCreaed;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class ProcessVideo implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\VideoCreaed  $event
     * @return void
     */
    public function handle(VideoCreaed $event)
    {
        dump($event->video);
        dump('this is process video listener');
    }


}
