@component('mail::message')
    # Verify Your Email

    Your order has been shipped!

    @component('mail::button', ['url' => $url])
        Verify Email
    @endcomponent

    Thanks,<br>
    {{ config('app.name') }}
@endcomponent
